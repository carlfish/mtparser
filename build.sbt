lazy val root = (project in file(".")).
  settings(
    name := "mtparser",
    version := "0.1",
    scalaVersion := "2.11.6",

    resolvers += "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases",
    
    libraryDependencies ++= Seq(
    	"org.scalaz.stream" %% "scalaz-stream" % "0.7a",
    	"org.json4s" %% "json4s-native" % "3.2.11")
  )
