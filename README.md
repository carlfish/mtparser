== DANGER, DANGER ==

This repository contains terrible code.

It was a quick and dirty way to get code out of my old Moveable Type
backup and into a bunch of JSON files, and I decided to make the
experience interesting by having it _also_ be my first foray into
scalaz and scalaz-stream BOTH AT THE SAME TIME, both of which were
overkill because this whole thing could probably have been done in
a tenth of the time with a few lines of Ruby.

There was no attempt to clean the code up or make it sensible after
determining that it (mostly) works.

There are many things hard-coded to the way my particular blog was
configured.
