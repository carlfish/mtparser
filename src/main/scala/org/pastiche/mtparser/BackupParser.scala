package org.pastiche.mtparser;

import scalaz.concurrent.Task
import scalaz._
import Scalaz._
import scalaz.stream._
import java.io._

import org.json4s._
import org.json4s.native.Serialization
import org.json4s.native.Serialization.writePretty

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This is ugly, but it works on the one piece of data it needs to work on so there's no
 * value in refactoring it to make it pretty.
 */
object MTParser {
	implicit val formats = Serialization.formats(NoTypeHints)		

	sealed trait Event
	case class Property(name: String, value: String) extends Event {
		if (name == "PS") throw new IllegalArgumentException(name + ": " + value)
	}
	case class NewSubObject(name: String) extends Event
	case class NewPost() extends Event

	def convert(src: String, destPath: String): Unit = 
		io.linesR(src)
			.pipe(parseToEvents)
			.pipe(parseToPosts)
			.pipe(routeToFiles(destPath))
			.to(writeFile).run.run

	def parseToEvents: Process1[String, Event] = {
		sealed trait PState
		case class StartSection() extends PState
		case class OneLineProperties() extends PState
		case class MultiLineProperty(name :String, values: Seq[String]) extends PState
		case class SubObject() extends PState
		case class SubObjectTrailer(lines: Seq[String]) extends PState

		val Propname = "^([A-Z](?:[A-Z ]*[A-Z])):$".r
		val Propline = "^([A-Z](?:[A-Z ]*[A-Z])): (.*)$".r
		val NewArticle = "^--------$".r
		val NewSection = "^-----$".r
		val BlankLine = "^\\s*$".r
		val NonProps = "^((?:PS|VLC):.*)$".r

		def join(lines: Seq[String]) = lines match {
			case Nil => ""
			case a :: Nil => a
			case a :: b => b.fold(a)((s1, s2) => s2 + "\n" + s1)
        }

		def endSubObject(bodyLines: Seq[String]) = Process.emit(Property("BODY", join(bodyLines)))

		def go(state: PState): Process1[String, Event] = Process.receive1[String, Event] { line =>
			state match {
				case (StartSection()) => line match {
					case BlankLine() => go(state)
					case Propname("COMMENT") => Process.emit(NewSubObject("COMMENT")) ++ go(SubObject())
					case Propname("PING") => Process.emit(NewSubObject("PING")) ++ go(SubObject())
					case NewArticle() => Process.emit(NewPost()) ++ go(StartSection())
					case NewSection() => go(state)
					case Propname(name) => go(MultiLineProperty(name, List()))
					case Propline(name, value) => Process.emit(Property(name, value)) ++ go(OneLineProperties())
					case s => throw new IllegalStateException("lol " + s)
				}
				case (OneLineProperties()) => line match {
					case BlankLine() => go(state)
					case Propline(name, value) => Process.emit(Property(name, value)) ++ go(OneLineProperties())
					case NewArticle() => Process.emit(NewPost()) ++ go(OneLineProperties())
					case NewSection() => go(StartSection())
					case s => throw new IllegalStateException("lol " + s)
				}
				case (MultiLineProperty(name, values)) => line match {
					case NewArticle() => Process.emit(Property(name, join(values))) ++ 
											Process.emit(NewPost()) ++ go(StartSection())
					case NewSection() => Process.emit(Property(name, join(values))) ++ go(StartSection())
					case s => go(MultiLineProperty(name, List(s) ++ values))
				}
				case (SubObject()) => line match {
					case NonProps(s) => go(SubObjectTrailer(List(s)))
					case Propline(name, value) => Process.emit(Property(name, value)) ++ go(state)
					case NewArticle() => Process.emit(NewPost()) ++ go(StartSection())
					case NewSection() => go(StartSection())
					case BlankLine() => go(state)
					case s => go(SubObjectTrailer(List(s)))
				}
				case (SubObjectTrailer(lines)) => line match {
					case NewArticle() => endSubObject(lines) ++ Process.emit(NewPost()) ++ go(StartSection())
					case NewSection() => endSubObject(lines) ++ go(StartSection())
					case s => go(SubObjectTrailer(List(s) ++ lines))
				}
				case _ => go(state)
			}
		}

		go(OneLineProperties())
	}

	type Converter = (String, Map[String, Any]) => Map[String, Any]
	def converter(mappedKey: String, f: String => Any): Converter = (s, m) => m + (mappedKey -> f(s))
	
	def ignore(key: String): (String, Converter) = (key, (v, m) => m)	
	def simple(key: String, mappedKey: String): (String, Converter) = (key, converter(mappedKey, identity))

	def strToBool(key: String, mappedKey: String): (String, Converter) = (key, converter(mappedKey, s => s match {
		case "0" => false
		case "1" => true
		case s => throw new IllegalStateException("Invalid boolean: " + s + " for " + key)
		}))

	def ignoreOrDie(key: String, test: String => Boolean): (String, Converter) = (key, (s, m) => if (test(s)) { 
			m 
		} else { 
			throw new IllegalArgumentException("Unexpected value: " + s + " for key " + key)
		})

	def convertBreaks(key: String): (String, Converter) = (key, converter("formatting", v => v match {
		case "0" => "html"
		case "1" => "convert_linebreaks"
		case "textile_2" => "textile"
		case "__default__" => "html"
		case s => throw new IllegalArgumentException("Unexpected convert breaks property: " + s)
	}))

	def appendToMapList(key: String, value: Any, m: Map[String, Any]) = m.updated(key, 
		m.getOrElse(key, List[Any]()) match {
			case l: List[Any] => value:: l
			case v => throw new IllegalStateException("Wrong type of thing:" + v)
		})

	def multi(key: String, mappedKey: String): (String, Converter) = (key, (s, m) => appendToMapList(mappedKey, s, m))

	def csv(key: String, mappedKey: String): (String, Converter) = (key, converter(mappedKey, _.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)")))

    val df: String = "MM/dd/yyyy hh:mm:ss aa" 
	def date(key: String, mappedKey: String): (String, Converter) = (key, converter(mappedKey, s => {
		val dft = new SimpleDateFormat(df)
		dft.setTimeZone(java.util.TimeZone.getTimeZone("Australia/Sydney"))
		dft.parse(s)
	}))
	val converters: Map[String, Converter] = Map(
		simple("AUTHOR", "author"),
		simple("TITLE", "title"),
		simple("BASENAME", "basename"),
		ignoreOrDie("STATUS", s => s == "Publish"),
		ignore("ALLOW COMMENTS"),
		ignore("ALLOW PINGS"),
		convertBreaks("CONVERT BREAKS"),
		simple("PRIMARY CATEGORY", "primaryCategory"),
		multi("CATEGORY", "categories"),
		simple("BODY", "body"),
		simple("EXTENDED BODY", "extendedBody"),
		simple("EXCERPT", "excerpt"),
		ignore("KEYWORDS"),
		date("DATE", "publishDate"),
		simple("EMAIL", "email"),
		simple("IP", "ip"),
		simple("URL", "url"),
		simple("BLOG NAME", "blogName"),
		csv("TAGS", "tags")
	);

	def parseToPosts: Process1[Event, Map[String, Any]] = {
		def newPost(post: Map[String, Any]) = if (post.isEmpty) goPost(post) else Process.emit(post) ++ goPost(Map())

		def newSubObject(name: String, post: Map[String, Any]) = name match {
			case "COMMENT" => goSub("comments", post, Map())
			case "PING" => goSub("pings", post, Map())
		}

		def addIfNotEmpty(key: String, v: Map[String, Any], post: Map[String, Any]) = if (v.isEmpty) post else appendToMapList(key, v, post)

		def goPost(post: Map[String, Any]): Process1[Event, Map[String, Any]] = 
			Process.receive1[Event, Map[String, Any]] { event => event match {
				case NewPost() => newPost(post)
				case Property(name, value) => goPost(converters(name)(value, post))
				case NewSubObject(name) => newSubObject(name, post)
			}}

		def goSub(key: String, post: Map[String, Any], sub: Map[String, Any]): Process1[Event, Map[String, Any]] = 
			Process.receive1[Event, Map[String, Any]] { event => event match {
				case NewPost() => newPost(addIfNotEmpty(key, sub, post))
				case Property(name, value) => goSub(key, post, converters(name)(value, sub))
				case NewSubObject(name) => newSubObject(name, addIfNotEmpty(key, sub, post))
			}}

		goPost(Map())
	}

	def routeToFiles(basedir: String): Process1[Map[String, Any], (String, String)] = 
			Process.receive1[Map[String, Any], (String, String)] { m =>
				val pathDateFormat = new SimpleDateFormat("yyyy/MMdd_")
				pathDateFormat.setTimeZone(java.util.TimeZone.getTimeZone("Australia/Sydney"))

				def findFreePath(basename: String, extension: String, i: Integer): String = {
					val p = basename + (if (i == 0) extension else "_" + i + extension)
					return if (! new File(p).exists) p else findFreePath(basename, extension, i + 1)
				}

				def doSubs(p: String, n: String, v: Option[Any]): List[(String, String)] = v match {
					case None => List()
					case Some(xs) => xs.asInstanceOf[List[Map[String, Any]]].map(x => 
						(findFreePath(p + "/" + n, ".json", 0) -> writePretty(x))) 
				}

				def doBody(path: String, ext: String, bs: Map[String, String]): (String, String) = {
					(path + "/body" + ext -> (bs.get("body").getOrElse("") + bs.get("extendedBody").map(s => 
						(if (s.trim.length > 0) "\n\n" + s else "")).getOrElse("")))
				}

				def formatToExt(ext: Option[Any]) = "." + ext.map(_.asInstanceOf[String]).getOrElse("html")

				val justPost = m - "comments" - "pings" - "body" - "extendedBody"
				val basename = basedir + "/" + (m.get("publishDate")
					.map(_.asInstanceOf[Date])
					.map(pathDateFormat.format(_))
					.flatMap({ s => 
						m.get("basename").map(_.asInstanceOf[String]).map(s + _)
					})
					.getOrElse {throw new IllegalStateException("basename or publishDate not found in: " + m.keys + "\n\n" + m)})

				val path = findFreePath(basename, "", 0)

				Process.emit(path + "/post.json" -> writePretty(justPost) ) ++
					Process.emit(doBody(path, formatToExt(m.get("formatting")), m.filterKeys(List("body", "extendedBody").contains).mapValues(_.asInstanceOf[String]))) ++
					Process.emitAll(doSubs(path, "comment", m.get("comments"))) ++
					Process.emitAll(doSubs(path, "ping", m.get(""))) ++
					routeToFiles(basedir)
			}

	def writeFile: Sink[Task, (String, String)] = sink.lift(toFile)

	def toFile: ((String, String)) => Task[Unit] = _ match {
		case (path: String, s: String) => Task {

			val toFile = new File(path)
			toFile.getParentFile.mkdirs

			val writer = new PrintWriter(toFile, "UTF-8")
			try {
				writer.write(s)
			} finally {
				writer.close
			}
		}
	}

}